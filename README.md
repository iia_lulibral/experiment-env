# decrypt-experiment-env

Supports x64 Debian-like architectures.


## installation procedure
These commands were tested on a *Linux mint 20 distribution*.

1. Clone the repository: ```git clone https://gitlab.limos.fr/iia_lulibral/experiment-env```
2. test the configuration (and solver installation):
    - gecode: ```external/minizinc-2-5-0/bin/minizinc -s --time-limit 3600000 models/test/test.mzn models/test/test.dzn```
    - chuffed: ```external/minizinc-2-5-0/bin/minizinc --solver external/solvers/chuffed/chuffed.msc -s --time-limit 3600000 models/test/test.mzn models/test/test.dzn```
    - ortools: ```external/minizinc-2-5-0/bin/minizinc --solver external/solvers/ortools/ortools.msc -s --time-limit 3600000 models/test/test.mzn models/test/test.dzn```
    - yuck: ```external/minizinc-2-5-0/bin/minizinc --solver external/solvers/yuck/yuck.msc -s --time-limit 3600000 models/test/test.mzn models/test/test.dzn```
    - gurobi: ```external/minizinc-2-5-0/bin/minizinc --solver gurobi --gurobi-dll /opt/gurobi910/lib/libgurobi91.so -s --time-limit 3600000 models/test/test.mzn models/test/test.dzn```
    - picat: ```external/minizinc-2-5-0/bin/minizinc -c models/test/test.mzn models/test/test.dzn && external/solvers/picat/picat external/solvers/picat/fzn_picat_sat.pi models/test/test.fzn```


## features

### solving technology

- [X] Minizinc installation
    - [X] gecode (should be installed by default with minizinc)
    - [X] chuffed
    - [X] OR-Tools
    - [X] Yuck
    - [X] gurobi (YES BUT: gurobi exec should be visible in PATH)
    - [X] picat3
    - [ ] choco
- [ ] OTHERS?

### models

- [ ] midori (Step1 opt)
    - [ ] minizinc model (no search annotations, no crypto options)
    - [ ] minizinc model (no search annotations, crypto options)
    - [ ] minizinc model (search annotations, no crypto options)
    - [ ] minizinc model (search annotations, crypto options)
    - [ ] picat model (no crypto options)
    - [ ] picat model (crypto options)
    - [ ] gurobi model (no crypto options)
    - [ ] gurobi model (crypto options)
    - [ ] choco XOR constraint (no crypto options)
    - [ ] choco XOR constraint (crypto options)
- [ ] midori (Step1 enum)
    - [ ] minizinc model (no search annotations, no crypto options)
    - [ ] minizinc model (no search annotations, crypto options)
    - [ ] minizinc model (search annotations, no crypto options)
    - [ ] minizinc model (search annotations, crypto options)
    - [ ] picat model (no crypto options)
    - [ ] picat model (crypto options)
    - [ ] gurobi model (no crypto options)
    - [ ] gurobi model (crypto options)
    - [ ] choco XOR constraint (no crypto options)
    - [ ] choco XOR constraint (crypto options)
- [ ] midori (Step2)
    - [ ] minizinc model
    - [ ] picat model
    - [ ] gurobi model
- [ ] AES


### instances

 - Midori64:
    - rounds: 1..20
 - Midori128:
    - rounds: 1..16
 - AES125:
    - rounds: 3..5
 - AES192:
    - rounds 3..10
 - AES256:
    - rounds 3..14

### scripts

- [ ] running script: for a job list, run each job, and outputs their output in a result directory. Stores the last commit ID and execution time for later use. Possibly, give the job the ability to store partial results.
- [ ] extract results and build latex result tables.


### minizinc possible options

- `-v` activates the verbose mode and displays more information (useful if using gurobi for instance to get more search statistics).
- `-c` compiles but not calls the compiler. Used to produce the .fzn files.


## directory architecture

- **external/** contains every external software/code
    - **minizinc-2-5-0/** currently used minizinc version
    - **solvers/** different minizinc solvers locally installed
- **models/** contains different models implemented in the postdoc


## useful links

 - https://www.minizinc.org/doc-2.4.3/en/solvers.html : solver installation procedure for minizinc
 - https://github.com/chocoteam/choco-parsers/blob/master/MINIZINC.md choco minizinc integration?
 - https://www.minizinc.org/challenge.html minizinc competition results

## useful commands

### Minizinc to Picat

1. generate the fzn file: `external/minizinc-2-5-0/bin/minizinc -c models/test/test.mzn models/test/test.dzn`
2. execute picat solver: `external/solvers/picat/picat external/solvers/picat/fzn_picat_sat.pi models/test/test.fzn`


## potential errors and fixes

for f in `find . -maxdepth 1 -regex '\./.*[^0-9][0-9]\.mzn.mzn'`; do echo tsp ../tagada/model_generators/runner_step1.py "${f}" ../experiment-env/ "${f}.json" picat 3600 a; done