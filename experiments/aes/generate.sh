#!/bin/sh
echo 'GENERATING AES128'
for R in 3 4 5
do
    ../../../cryptodag/generate_aes_model.rb 128 "$R" "diff_aes128_$R.mzn"
done
echo 'GENERATING AES256'
for R in 3 4 5 6 7 8 9 10 11 12 13 14
do
    ../../../cryptodag/generate_aes_model.rb 256 "$R" "diff_aes256_$R.mzn"
done