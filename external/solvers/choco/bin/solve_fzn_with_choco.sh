#!/bin/zsh
shift
echo $* >&2
BASE="$(dirname $0)/.."

CHOCO_LIBS="$BASE/lib"
CHOCO_PARSER=$(printf "$CHOCO_LIBS"/choco-parsers-*-jar-with-dependencies.jar)

MINIZINC_BASE="$BASE/MiniZincIDE-2.5.5-bundle-linux-x86_64"
MINIZINC_LIB="$MINIZINC_BASE/lib"
MINIZINC_BIN="$MINIZINC_BASE/bin"

error() {
    printf "$*" >&2
    exit 1
}

doc() {
    cat <<EOF
Usage:
$(basename $0) Minizinc_or_flatzinc_file  # For model contained in a signe file
$(basename $0) Minizinc_or_flatzinc_files...  # All files will be cat altogether in a single file
EOF
    exit 9
}

mzn2fzn() {
    local mzn="$1"
    local fzn=$(echo $mzn | sed -re s/mzn$/fzn/)
    local log="/tmp/mzn2fzn_$(basename $mzn)"

    if "$MINIZINC_BIN/minizinc" -c --solver org.minizinc.mzn-fzn -I "$MINIZINC_LIB" "$mzn" -o "$fzn" &> "$log"; then
        echo "$fzn"
    else
        return 1
    fi
}

case $# in
    0)
        doc;;
    1)
        FZN_FILE="$1";;
    *)
        FZN_FILE=$(echo /tmp/ $* | sed -re 's/\.mzn//' | sed -re 's/ +/-/' )
        cat $* > "$FZN_FILE"
        ;;
esac


if printf "$FZN_FILE" | grep -q "\.mzn"; then
    FZN_process=$(mzn2fzn $FZN_FILE) || { echo "Can't translate $FZN_FILE to FZN"; exit 1 }
else
    FZN_process="$FZN_FILE"
fi

if java -cp ".:$CHOCO_PARSER" org.chocosolver.parser.flatzinc.ChocoFZN "$FZN_process"; then
    echo "Choco for $FZN_FILE successful">&2
else
    echo "Choco for $FZN_FILE failed" >&2
fi
# &> "/tmp/choco_$(basename $FZN_process)"
