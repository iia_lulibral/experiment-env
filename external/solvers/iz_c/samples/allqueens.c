#include <stdlib.h>
#include <time.h>

#include "iz.h"

IZBOOL knownQueen(int val, int index, CSint **allvars, int NbQueens, void *extra)
{
  int i;

  for (i = 0; i < NbQueens; i++)
    if (i != index) {
      CSint *var = allvars[i];

      if (!cs_NEQ(var, val)) return FALSE;
      if (!cs_NEQ(var, val + index - i)) return FALSE;
      if (!cs_NEQ(var, val + i - index)) return FALSE;
    }
  return TRUE;
}

void printSolution(CSint **allvars, int nbVars)
{
  static int NbSolutions = 0;

  printf("Solution %d (%d fails):\n", ++NbSolutions, cs_getNbFails());
  cs_printf("%A\n\n", allvars, nbVars);
}

int main(int argc, char **argv)
{
  clock_t t0 = clock();
  int NbQueens = (argc > 1) ? atoi(argv[1]) : 8;
  CSint **allvars;
  cs_init();
  allvars = cs_createCSintArray(NbQueens, 1, NbQueens);

  printf("\n-------------------- %s %d --------------------\n", argv[0], NbQueens);

  cs_eventKnown(allvars, NbQueens, knownQueen, NULL);
  cs_findAll(allvars, NbQueens, cs_findFreeVarNbElementsMin, printSolution);

  cs_printStats();
  printf("Elapsed Time = %fs\n", (double) (clock() - t0) / CLOCKS_PER_SEC);
  cs_end();

  return 0;
}
