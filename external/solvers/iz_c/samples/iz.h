/*                                                           */
/* iZ-C : a Constraint Library in C                          */
/*                   by NTT DATA SEKISUI SYSTEMS CORPORATION */
/*                                                           */
/* iz.h : API header file for iZ-C                           */
/*                                                           */
/* Copyright (c) 2012 NTT DATA SEKISUI SYSTEMS CORPORATION   */
/*                                                           */

#ifndef __izH
#define __izH

#include <stdio.h>
#include <limits.h>

#ifdef __izwindllbuild
#define __izwindllexport __declspec(dllexport)
#else
#define __izwindllexport
#endif

#ifdef	__cplusplus
extern "C" {
#endif

#ifndef FALSE
#define FALSE 0
#endif /* FALSE */

#ifndef TRUE
#define TRUE 1
#endif /* TRUE */

struct _rcsInt;
typedef struct _rcsInt CSint;
typedef char IZBOOL;

struct _rcsValueSelector;
typedef struct _rcsValueSelector CSvalueSelector;

typedef struct _rcsNoGood CSnoGood;

typedef struct _rcsSearchNotify CSsearchNotify;
  
typedef struct {
	int method;
	int value;
} CSvalueSelection;

typedef struct _rcsNoGoodSet CSnoGoodSet;
	
/* Domain Variable */
extern __izwindllexport CSint *cs_createCSint(int min, int max);
extern __izwindllexport CSint *cs_createNamedCSint(int min, int max, char *name);
extern __izwindllexport CSint *CSINT(int n);
extern __izwindllexport CSint *cs_createCSintFromDomain(int *tcoeff, int size);
extern __izwindllexport CSint **cs_createCSintArray(int size, int min, int max);
extern __izwindllexport void cs_setName(CSint *vint, char *name);
extern __izwindllexport IZBOOL cs_isFree(CSint* vint);
extern __izwindllexport IZBOOL cs_isInstantiated(CSint* vint);
extern __izwindllexport int cs_getMin(CSint* vint);
extern __izwindllexport int cs_getMax(CSint* vint);
extern __izwindllexport int cs_getNbElements(CSint* vint);
extern __izwindllexport int cs_getNbConstraints(CSint* vint);
extern __izwindllexport char* cs_getName(CSint* vint);
extern __izwindllexport int cs_getValue(CSint *vint);
extern __izwindllexport int cs_getNextValue(CSint *vint, int val);
extern __izwindllexport int cs_getPreviousValue(CSint *vint, int val);
extern __izwindllexport int *cs_getDomain(CSint *vint);
extern __izwindllexport void cs_freeDomain(int* domain);
extern __izwindllexport IZBOOL cs_isIn(CSint *vint, int val);
extern __izwindllexport void cs_printf(const char *control, ...);
extern __izwindllexport void cs_fprintf(FILE *f, const char *control, ...);
/* In */
extern __izwindllexport IZBOOL cs_InArray(CSint *vint, int *array, int size);
extern __izwindllexport IZBOOL cs_NotInArray(CSint *vint, int *array, int size);
extern __izwindllexport IZBOOL cs_InInterval(CSint *vint, int min, int max);
extern __izwindllexport IZBOOL cs_NotInInterval(CSint *vint, int min, int max);
extern __izwindllexport IZBOOL cs_AllNeq(CSint **tint, int size);
/* Len */
extern __izwindllexport IZBOOL cs_Eq(CSint *vint1, CSint *vint2);
extern __izwindllexport IZBOOL cs_EQ(CSint *vint, int val);
extern __izwindllexport IZBOOL cs_Neq(CSint *vint1, CSint *vint2);
extern __izwindllexport IZBOOL cs_NEQ(CSint *vint, int val);
extern __izwindllexport IZBOOL cs_Le(CSint *vint1, CSint *vint2);
extern __izwindllexport IZBOOL cs_LE(CSint *vint, int val);
extern __izwindllexport IZBOOL cs_Ge(CSint *vint1, CSint *vint2);
extern __izwindllexport IZBOOL cs_GE(CSint *vint, int val);
extern __izwindllexport IZBOOL cs_Lt(CSint *vint1, CSint *vint2);
extern __izwindllexport IZBOOL cs_LT(CSint *vint, int val);
extern __izwindllexport IZBOOL cs_Gt(CSint *vint1, CSint *vint2);
extern __izwindllexport IZBOOL cs_GT(CSint *vint, int val);
/* arith */
extern __izwindllexport CSint *cs_Add(CSint *vint1, CSint *vint2);
extern __izwindllexport CSint *cs_VAdd(int nbVars, CSint *vint, ...);
extern __izwindllexport CSint *cs_Sub(CSint *vint1, CSint *vint2);
extern __izwindllexport CSint *cs_VSub(int nbVars, CSint *vint, ...);
extern __izwindllexport CSint *cs_Mul(CSint *vint1, CSint *vint2);
extern __izwindllexport CSint *cs_VMul(int nbVars, CSint *vint, ...);
extern __izwindllexport CSint *cs_Div(CSint *vint1, CSint *vint2);
extern __izwindllexport CSint *cs_VDiv(int nbVars, CSint *vint, ...);
extern __izwindllexport CSint *cs_Sigma(CSint **tint, int size);
extern __izwindllexport CSint *cs_ScalProd(CSint **tint, int *tcoeff, int size);
extern __izwindllexport CSint *cs_VScalProd(int nbVars, CSint *vint, ...);
/* abs */
extern __izwindllexport CSint *cs_Abs(CSint *vint);
/* minmax */
extern __izwindllexport CSint *cs_Min(CSint **tint, int size);
extern __izwindllexport CSint *cs_VMin(int nbVars, CSint *vint, ...);
extern __izwindllexport CSint *cs_Max(CSint **tint, int size);
extern __izwindllexport CSint *cs_VMax(int nbVars, CSint *vint, ...);
/* if */
extern __izwindllexport IZBOOL cs_IfEq(CSint *vint1, CSint *vint2, int val1, int val2);
extern __izwindllexport IZBOOL cs_IfNeq(CSint *vint1, CSint *vint2, int val1, int val2);
/* occur */
extern __izwindllexport CSint *cs_Occur(CSint *vint, int val, CSint **tint, int size);
extern __izwindllexport CSint *cs_OccurDomain(int val, CSint **tint, int size);
extern __izwindllexport IZBOOL cs_OccurConstraints(CSint *vint, int val, CSint **tint, int size);
/* index */
extern __izwindllexport CSint *cs_Index(CSint **tint, int size, int val);
/* element */
extern __izwindllexport CSint *cs_Element(CSint *index, int *values, int size);
extern __izwindllexport CSint *cs_VarElement(CSint *index, CSint **tint, int size);
extern __izwindllexport CSint *cs_VarElementRange(CSint *index, CSint **tint, int size);
/* cumulative */
extern __izwindllexport IZBOOL cs_Cumulative(CSint** startVars, CSint** durationVars, CSint** resourceVars, int size, CSint* limitVar);
/* disjunctive */
extern __izwindllexport IZBOOL cs_Disjunctive(CSint** startVars, CSint** durationVars, int size);
/* reif */
extern __izwindllexport CSint *cs_ReifEq(CSint *vint1, CSint *vint2);
extern __izwindllexport CSint *cs_ReifEQ(CSint *vint, int val);
extern __izwindllexport CSint *cs_ReifNeq(CSint *vint1, CSint *vint2);
extern __izwindllexport CSint *cs_ReifNEQ(CSint *vint, int val);
extern __izwindllexport CSint *cs_ReifLt(CSint *vint1, CSint *vint2);
extern __izwindllexport CSint *cs_ReifLT(CSint *vint, int val);
extern __izwindllexport CSint *cs_ReifLe(CSint *vint1, CSint *vint2);
extern __izwindllexport CSint *cs_ReifLE(CSint *vint, int val);
extern __izwindllexport CSint *cs_ReifGt(CSint *vint1, CSint *vint2);
extern __izwindllexport CSint *cs_ReifGT(CSint *vint, int val);
extern __izwindllexport CSint *cs_ReifGe(CSint *vint1, CSint *vint2);
extern __izwindllexport CSint *cs_ReifGE(CSint *vint, int val);
/* Search */
extern __izwindllexport void cs_printStats(void);
extern __izwindllexport void cs_fprintStats(FILE *f);
extern __izwindllexport CSint* cs_findFreeVar(CSint **allvars, int nbVars);
extern __izwindllexport CSint* cs_findFreeVarNbElements(CSint **allvars, int nbVars);
extern __izwindllexport CSint* cs_findFreeVarNbElementsMin(CSint **allvars, int nbVars);
extern __izwindllexport CSint* cs_findFreeVarNbConstraints(CSint **allvars, int nbVars);
extern __izwindllexport int cs_getNbFails(void);
extern __izwindllexport int cs_getNbChoicePoints(void);
extern __izwindllexport void cs_cancelSearch(void);
extern __izwindllexport IZBOOL cs_search(CSint **allvars, int nbVars, CSint* (*findFreeVar)(CSint **allvars, int nbVars));
extern __izwindllexport IZBOOL cs_searchFail(CSint **allvars, int nbVars, CSint*
			  (*findFreeVar)(CSint **allvars, int nbVars),
			  int NbFailsMax);
extern __izwindllexport IZBOOL cs_searchCriteria(CSint **allvars, int nbVars,
			      int (*findFreeVar)(CSint **allvars, int nbVars),
			      int (*criteria)(int index, int val));
extern __izwindllexport IZBOOL cs_searchCriteriaFail(CSint **allvars, int nbVars,
				  int (*findFreeVar)(CSint **allvars, int nbVars),
				  int (*criteria)(int index, int val),
				  int NbFailsMax);
extern __izwindllexport IZBOOL cs_searchMatrix(CSint ***matrix, int NbRows, int NbCols,
			    int (*findFreeRow)(CSint ***matrix, int NbRows, int NbCols),
			    int (*findFreeCol)(int row, CSint **Row, int NbCols),
			    int (*criteria)(int row, int col, int val));
extern __izwindllexport IZBOOL cs_searchMatrixFail(CSint ***matrix, int NbRows, int NbCols,
				int (*findFreeRow)(CSint ***matrix, int NbRows, int NbCols),
				int (*findFreeCol)(int row, CSint **Row, int NbCols),
				int (*criteria)(int row, int col, int val),
				int NbFailsMax);
extern __izwindllexport IZBOOL cs_Vsearch(int nbVars, CSint *vint, ...);
extern __izwindllexport IZBOOL cs_searchValueSelectorFail(CSint** allvars,
			   const CSvalueSelector** selectors,
			   int nbVars,
			   int (*findFreeVar)(CSint** allvars, int nbVars),
			   int NbFailsMax,
			   const CSsearchNotify* notify);
extern __izwindllexport IZBOOL cs_searchValueSelectorRestartNG(CSint** allvars,
			   const CSvalueSelector** selectors,
			   int nbVars,
			   int (*findFreeVar)(CSint** allvars, int nbVars),
			   int (*maxFailFunction)(void* state),
			   void* maxFailState,
			   int nbFailsMax,
			   CSnoGoodSet* ngs,
			   const CSsearchNotify* notify);

extern __izwindllexport IZBOOL cs_findAll(CSint **allvars, int nbVars, CSint* (*findFreeVar)(CSint **allvars, int nbVars),
		       void (*found)(CSint **allvars, int nbVars));
extern __izwindllexport IZBOOL cs_minimize(CSint **allvars, int nbVars, CSint* (*findFreeVar)(CSint **allvars, int nbVars), CSint *cost,
			void (*found)(CSint **allvars, int nbVars, CSint *cost));

/* Value selector */	
extern __izwindllexport const CSvalueSelector* cs_getValueSelector(int vs);
extern __izwindllexport CSvalueSelector* cs_createValueSelector(
	IZBOOL (*init)(int index, CSint** vars, int size, void* pData),
	IZBOOL (*next)(CSvalueSelection* r, int index, CSint** vars, int size, void* pData),
	IZBOOL (*end)(int index, CSint** vars, int size, void* pData));
extern __izwindllexport void cs_freeValueSelector(CSvalueSelector* vs);
extern __izwindllexport IZBOOL cs_initValueSelector(const CSvalueSelector* vs,
	int index, CSint**vars, int size, void* pData);
extern __izwindllexport IZBOOL cs_selectNextValue(CSvalueSelection* r,
	const CSvalueSelector* vs, int index, CSint** vars, int size, void* pData);

extern __izwindllexport IZBOOL cs_endValueSelector(const CSvalueSelector* vs,
	int index, CSint** vars, int size, void* pData);
extern __izwindllexport IZBOOL cs_selectValue(CSint* v, const CSvalueSelection* vs);	

/* Demon */
extern __izwindllexport IZBOOL cs_eventAllKnown(CSint **tint, int size,
			     IZBOOL (*allKnown)(CSint **tint, int size, void *extra),
			     void *extra);
extern __izwindllexport IZBOOL cs_eventKnown(CSint **tint, int size,
			  IZBOOL (*known)(int val, int index, CSint **tint, int size, void *extra),
			  void *extra);
extern __izwindllexport void cs_eventNewMin(CSint **tint, int size,
			   IZBOOL (*newMin)(CSint *vint, int index, int oldMin, CSint **tint, int size, void *extra),
			   void *extra);
extern __izwindllexport void cs_eventNewMax(CSint **tint, int size,
			   IZBOOL (*newMax)(CSint *vint, int index, int oldMax, CSint **tint, int size, void *extra),
			   void *extra);
extern __izwindllexport void cs_eventNeq(CSint **tint, int size,
			IZBOOL (*neq)(CSint *vint, int index, int neqValue, CSint **tint, int size, void *extra), 
			void *extra);
extern __izwindllexport void cs_eventConflict(CSint **tint, int size,
			void (*conflict)(CSint *vint, int index, const CSvalueSelection* vs, CSint **tint, int size, void *extra), 
			void *extra);

/* Context */
extern __izwindllexport void cs_backtrack(CSint *vint, int index, void (*backtrack)(CSint *vint, int index));
extern __izwindllexport void cs_backtrackExt(CSint* vint, void* ext, void (*backtrack)(CSint *vint, void* ext));

extern __izwindllexport int cs_saveContext(void);
extern __izwindllexport void cs_forgetSaveContext(void);
extern __izwindllexport void cs_restoreAndSaveContext(void);
extern __izwindllexport void cs_acceptContext(void);
extern __izwindllexport void cs_acceptAll(void);
extern __izwindllexport void cs_restoreContext(void);
extern __izwindllexport void cs_restoreAll(void);
extern __izwindllexport void cs_init(void);
extern __izwindllexport void cs_end(void);
extern __izwindllexport void cs_forgetSaveContextUntil(int label);
extern __izwindllexport void cs_acceptContextUntil(int label);
extern __izwindllexport void cs_restoreContextUntil(int label);
extern __izwindllexport void cs_initErr(void);
extern __izwindllexport int cs_getErr(void);
extern __izwindllexport void cs_setErr(int code);
extern __izwindllexport void cs_setErrHandler(int code, void (*func)(void* data, void* ext), void* ext);

/* No Good */
extern __izwindllexport CSnoGoodSet* cs_createNoGoodSet(CSint** vars, int size,
							IZBOOL (*prefilter)(CSnoGoodSet* ngs, CSnoGood* ng, CSint** vars, int size, void* ext),
							int maxNoGood,
							void(*destructorNotify)(CSnoGoodSet* ngs, void* ext), void* ext);

extern __izwindllexport int cs_getNbNoGoods(CSnoGoodSet* ngs);
extern __izwindllexport int cs_getNbNoGoodElements(CSnoGood* ng);
extern __izwindllexport void cs_getNoGoodElementAt(int* pIndex, CSvalueSelection* vs, CSnoGood* ng, int index);
extern __izwindllexport void cs_filterNoGood(CSnoGoodSet* ngs, IZBOOL (*filter)(CSnoGoodSet* ngs, CSnoGood* elem, CSint** vars, int size, void* ext));
extern __izwindllexport void cs_addNoGood(CSnoGoodSet* ngs, int* index, CSvalueSelection* vs, int size);
extern __izwindllexport void cs_setMaxNbNoGoods(CSnoGoodSet* ngs, int max);

/* Search Notify */
extern __izwindllexport CSsearchNotify* cs_createSearchNotify(void* ext);
extern __izwindllexport void cs_freeSearchNotify(CSsearchNotify* notify);
extern __izwindllexport void cs_searchNotifySetSearchStart(CSsearchNotify* notify, void (*searchStart)(int maxFails, CSint** allvars, int nbVars, void* ext));
extern __izwindllexport void cs_searchNotifySetSearchEnd(CSsearchNotify* notify, void (*searchEnd)(IZBOOL result, int nbFails, int maxFails, CSint** allvars, int nbVars, void* ext));
extern __izwindllexport void cs_searchNotifySetBeforeValueSelection(CSsearchNotify* notify, void (*beforeValueSelection)(int depth, int index, const CSvalueSelection* vs, CSint** allvars, int nbVars, void* ext));
extern __izwindllexport void cs_searchNotifySetAfterValueSelection(CSsearchNotify* notify, void (*afterValueSelection)(IZBOOL result, int depth, int index, const CSvalueSelection* vs, CSint** allvars, int nbVars, void* ext));
extern __izwindllexport void cs_searchNotifySetEnter(CSsearchNotify* notify, void (*enter)(int depth, int index, CSint** allvars, int nbVars, void* ext));
extern __izwindllexport void cs_searchNotifySetLeave(CSsearchNotify* notify, void (*leave)(int depth, int index, CSint** allvars, int nbVars, void* ext));
extern __izwindllexport void cs_searchNotifySetFound(CSsearchNotify* notify, IZBOOL (*found)(int depth, CSint** allvars, int nbVars, void* ext));

extern __izwindllexport const char* cs_getVersion(void);

/* Error code */
#define CS_ERR_NONE		0
#define CS_ERR_GETVALUE		-1
#define CS_ERR_OVERFLOW		-2
#define CS_ERR_NO_MEMORY	-3

/* Value selector */
#define CS_VALUE_SELECTOR_MIN_TO_MAX		0
#define CS_VALUE_SELECTOR_MAX_TO_MIN		1
#define CS_VALUE_SELECTOR_LOWER_AND_UPPER	2
#define CS_VALUE_SELECTOR_UPPER_AND_LOWER	3
#define CS_VALUE_SELECTOR_MEDIAN_AND_REST	4

#define CS_VALUE_SELECTION_EQ	0
#define CS_VALUE_SELECTION_NEQ	1
#define CS_VALUE_SELECTION_LE	2
#define CS_VALUE_SELECTION_LT	3
#define CS_VALUE_SELECTION_GE	4
#define CS_VALUE_SELECTION_GT	5

/*  Version */
#define IZ_VERSION_MAJOR	3
#define IZ_VERSION_MINOR	6
#define IZ_VERSION_PATCH	2
	
#ifdef	__cplusplus
}
#endif

#endif
