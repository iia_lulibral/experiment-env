#include <stdlib.h>
#include <time.h>
#include "iz.h"

#define N 15

int row_01[] = {2,	1, 1};
int row_02[] = {2,	2, 2};
int row_03[] = {1,	9};
int row_04[] = {2,	4, 6};
int row_05[] = {2,	1, 7};

int row_06[] = {4,	3, 1, 1, 5};
int row_07[] = {4,	1, 1, 2, 3};
int row_08[] = {2,	2, 5};
int row_09[] = {2,	3, 4};
int row_10[] = {2,	4, 1};

int row_11[] = {2,	3, 1};
int row_12[] = {1,	10};
int row_13[] = {3,	2, 3, 2};
int row_14[] = {1,	7};
int row_15[] = {1,	5};


int col_01[] = {1,	3};
int col_02[] = {2,	1, 1};
int col_03[] = {2,	1, 7};
int col_04[] = {2,	4, 6};
int col_05[] = {2,	2, 6};

int col_06[] = {5,	1, 2, 1, 1, 2};
int col_07[] = {2,	1, 4};
int col_08[] = {3,	1, 2, 4};
int col_09[] = {3,	1, 5, 4};
int col_10[] = {4,	3, 2, 1, 2};

int col_11[] = {2,	8, 3};
int col_12[] = {1,	13};
int col_13[] = {1,	4};
int col_14[] = {1,	3};
int col_15[] = {1,	3};

int *row[N] = {row_01,
	       row_02,
	       row_03,
	       row_04,
	       row_05,
	       row_06,
	       row_07,
	       row_08,
	       row_09,
	       row_10,
	       row_11,
	       row_12,
	       row_13,
	       row_14,
	       row_15};

int *col[N] = {col_01,
	       col_02,
	       col_03,
	       col_04,
	       col_05,
	       col_06,
	       col_07,
	       col_08,
	       col_09,
	       col_10,
	       col_11,
	       col_12,
	       col_13,
	       col_14,
	       col_15};

struct Tarray {
  int *_array;
  int _arraySize;
};

int getIndex(int index, CSint **tint, int size)
{
  int i = 0;

  index--;
  while ((index >= 0) && cs_isInstantiated(tint[index]) && (cs_getValue(tint[index]) == 1))
    index--;
  if (index < 0)
    return i;
  if (cs_isFree(tint[index]))
    return -1;
  while (index >= 0) {
    while ((index >= 0) && cs_isInstantiated(tint[index]) && (cs_getValue(tint[index]) == 0))
      index--;
    if (index < 0)
      return i;
    if (cs_isFree(tint[index]))
      return -1;
    index--;
    i++;
    while ((index >= 0) && cs_isInstantiated(tint[index]) && (cs_getValue(tint[index]) == 1))
      index--;
    if (index < 0)
      return i;
    if (cs_isFree(tint[index]))
      return -1;
    index--;
  }
  return i;
}

IZBOOL known(int val, int index, CSint **tint, int size, void *Sarray)
{
  int *array = ((struct Tarray*) Sarray)->_array;
  int arraySize = ((struct Tarray*) Sarray)->_arraySize;

  if (val == 1) {
    int i = getIndex(index, tint, size);
    int j, nGoal, nCons;
    if (i < 0)
      return TRUE;
    if (i >= arraySize)
      return FALSE;
    nGoal = array[i];
    nCons = 1;
    j = index - 1;
    while ((j >= 0) && (cs_getValue(tint[j]) == 1)) {
      j--;
      nCons++;
    }
    j = index + 1;
    while ((j < size) && cs_isInstantiated(tint[j]) && (cs_getValue(tint[j]) == 1)) {
      j++;
      nCons++;
    }
    if (nCons > nGoal)
      return FALSE;
    if (nCons == nGoal)
      if (j < size)
	return(cs_EQ(tint[j], 0));
    if (nCons < nGoal) {
      while ((j < size) && (nCons < nGoal)) {
	if (!cs_EQ(tint[j], 1))
	  return FALSE;
	j++;
	nCons++;
      }
      if (nCons != nGoal)
	return FALSE;
      if (j < size)
	return(cs_EQ(tint[j], 0));
    }
  }
  return TRUE;
}

char Mcons(CSint **tint, int size, int *array, int arraySize)
{
  struct Tarray *Sarray = (struct Tarray*) malloc(sizeof(struct Tarray));
  int i, s = 0;

  for (i = 0; i < arraySize; i++)
    s += array[i];

  Sarray->_array = array;
  Sarray->_arraySize = arraySize;

  return(cs_EQ(cs_Sigma(tint, size), s) &&
	 cs_eventKnown(tint, size, known, (void*) Sarray));
}

void printSolution(CSint **allvars, int nbVars)
{
  int i, j, n = 0;
  static int NbSolution = 0;

  printf("\nSolution %d (NbFails = %d):\n", ++NbSolution, cs_getNbFails());
  for (i = 0; i < N; i++) {
    for (j = 0; j < N; j++)
      cs_printf("%D ", allvars[n++]);
    printf("\n");
  }
}

void fail()
{
  fprintf(stderr, "Fail!\n");	
  exit(-1);
}

int main(int argc, char **argv)
{
  clock_t t0 = clock();
  CSint *matrixRC[N][N];
  CSint *matrixCR[N][N];
  CSint *allvars[N * N];
  int i, j, n = 0;

  printf("\n-------------------- %s --------------------\n", argv[0]);

  cs_init();

  for (i = 0; i < N; i++)
    for (j = 0; j < N; j++)
      allvars[n++] = matrixCR[j][i] = matrixRC[i][j] = cs_createCSint(0, 1);

  for (i = 0; i < N; i++) {
    if (!Mcons(matrixRC[i], N, &row[i][1], row[i][0])) fail();
    if (!Mcons(matrixCR[i], N, &col[i][1], col[i][0])) fail();
  }

  if (!cs_findAll(allvars, N * N, cs_findFreeVar, printSolution))
    printf("No solution\n");

  cs_printStats();
  cs_end();
  printf("Elapsed Time = %fs\n", (double) (clock() - t0) / CLOCKS_PER_SEC);

  return 0;
}
