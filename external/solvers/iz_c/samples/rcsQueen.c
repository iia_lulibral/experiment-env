#include <stdlib.h>
#include <time.h>

#include "iz.h"

IZBOOL knownQueen(int val, int index, CSint **allvars, int NbQueens, void *extra){
	int i;
	for (i = 0; i < NbQueens; i++){
		if (i != index) {
			CSint *var = allvars[i];
			if (!cs_NEQ(var, val)) return FALSE;
			if (!cs_NEQ(var, val + index - i)) return FALSE;
			if (!cs_NEQ(var, val + i - index)) return FALSE;
		}
	}
	return TRUE;
}

int main(int argc, char **argv){
	clock_t t0 = clock();
	int i;
	int NbQueens = (argc > 1) ? atoi(argv[1]) : 100;
	CSint **allvars;
	printf("\n-------------------- %s %d --------------------\n", argv[0], NbQueens);
	cs_init();
	allvars = cs_createCSintArray(NbQueens, 0, NbQueens - 1);
	cs_eventKnown(allvars, NbQueens, knownQueen, NULL);
	cs_search(allvars, NbQueens, cs_findFreeVarNbElementsMin);
	for (i = 0; (i < NbQueens) && (i <= 100); i++){
		cs_printf("%D",allvars[i]);
		printf(" ");
	}
	if (NbQueens > 100)
		printf("...");
	printf("\n");
	cs_printStats();
	cs_end();
	printf("Elapsed Time = %fs\n", (double) (clock() - t0) / CLOCKS_PER_SEC);
	return 0;
}
