#include "iz.h"

static IZBOOL knownVint(int val, int index, CSint **tint, int size, void *s)
{
  if (val < 0)
    return(cs_EQ((CSint*) s, -1));
  if (val > 0)
    return(cs_EQ((CSint*) s, 1));
  return(cs_EQ((CSint*) s, 0));
}

static IZBOOL newMinVint(CSint *vint, int index, int oldMin, CSint **array, int size, void *s)
{
  if (cs_getMin(vint) == 0)
    return(cs_GE((CSint*) s, 0));
  if (cs_getMin(vint) > 0)
    return(cs_GT((CSint*) s, 0));
  return TRUE;
}

static IZBOOL newMaxVint(CSint *vint, int index, int oldMin, CSint **array, int size, void *s)
{
  if (cs_getMax(vint) == 0)
    return(cs_LE((CSint*) s, 0));
  if (cs_getMax(vint) < 0)
    return(cs_LT((CSint*) s, 0));
  return TRUE;
}

static IZBOOL neqVint(CSint *vint, int index, int neqValue, CSint **array, int size, void *s)
{
  if (neqValue == 0)
    return(cs_NEQ((CSint*) s, 0));
  return TRUE;
}

static IZBOOL knownS(int valS, int index, CSint **tint, int size, void *vint)
{
  if (valS < 0)
    return(cs_LT((CSint*) vint, 0));
  if (valS > 0)
    return(cs_GT((CSint*) vint, 0));
  return(cs_EQ((CSint*) vint, 0));
}

static IZBOOL newMinS(CSint *s, int index, int oldMin, CSint **array, int size, void *vint)
{
  return(cs_GE((CSint*) vint, 0));
}

static IZBOOL newMaxS(CSint *s, int index, int oldMin, CSint **array, int size, void *vint)
{
  return(cs_LE((CSint*) vint, 0));
}

static IZBOOL neqS(CSint *s, int index, int neqValue, CSint **array, int size, void *vint)
{
  return(cs_NEQ((CSint*) vint, 0));
}

CSint *Sgn(CSint *vint)
{
  int n = 0;
  int array[3];
  CSint *s;

  if (cs_getMin(vint) < 0)
    array[n++] = -1;
  if (cs_getMax(vint) > 0)
    array[n++] = 1;
  if (cs_isIn(vint, 1))
    array[n++] = 0;
  s = cs_createCSintFromDomain(array, n);

  cs_eventKnown(&vint, 1, knownVint, (void*) s);
  cs_eventNewMin(&vint, 1, newMinVint, (void*) s);
  cs_eventNewMax(&vint, 1, newMaxVint, (void*) s);
  cs_eventNeq(&vint, 1, neqVint, (void*) s);

  cs_eventKnown(&s, 1, knownS, (void*) vint);
  cs_eventNewMin(&s, 1, newMinS, (void*) vint);
  cs_eventNewMax(&s, 1, newMaxS, (void*) vint);
  cs_eventNeq(&s, 1, neqS, (void*) vint);

  return(s);
}

int main(int argc, char **argv)
{
  CSint *A, *B;

  printf("\n-------------------- %s --------------------\n", argv[0]);
  cs_init();

  A = cs_createNamedCSint(-10, 10, "A");
  B = cs_createNamedCSint(-10, 10, "B");

  cs_Eq(cs_Add(Sgn(A), Sgn(B)), cs_Mul(A, B));
  cs_NEQ(A, 0);

  cs_Vsearch(2, A, B, cs_findFreeVar);
  cs_printf("%T\n%T\n", A, B);

//  printf("size = %d\n", sizeof(CSint));
  cs_printStats();
  cs_end();
  return 0;
}

