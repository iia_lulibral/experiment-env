# MiniSAT

To provided executable requires two positional arguments `input-file` and `result-output-file`:
- `input-file` corresponds to the input file containing the CNF clauses in DIMACS format.
- `result-output-file` corresponds to the output file.
