#!/usr/bin/python3

import sys
import getopt
import os
import subprocess

def main(argv):
    try:
        opts, args = getopt.getopt(argv, "s:", ["flatzinc"])
    except getopt.GetoptError:
        print('ERROR parsing arguments (picatMinizinc.py)')
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-s"):
            dirname = os.path.dirname(os.path.realpath(__file__))
            inputfile = os.getcwd()+"/"+arg
            # process = subprocess.Popen([
            #     "{}/picat".format(dirname),
            #     "{}/fzn_picat_sat.pi".format(dirname),
            #     inputfile
            # ], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            # stdout, stderr = process.communicate()
            # res = stdout.decode("utf-8").split("\n")
            # for e in res:
            #     if len(e) > 0 and e[0] != "%":
            #         print(e)
            os.system("{}/picat {}/fzn_picat_sat.pi {}".format(dirname, dirname, inputfile))

if __name__ == "__main__":
    main(sys.argv[1:])