#!/usr/bin/python
from sys import argv
import os.path
import json

solvers = ["gurobi", "picat", "gecode", "chuffed", "ortools"]
instances = ["midori_{}".format(i) for i in range(3,21)]

def read_json(filename):
    if not os.path.isfile(filename):
        return {"status": "error", "why": "file not found"}
    with open(filename, "r") as f:
        return json.load(f)


def main(dirname):
    data = {}
    for s in solvers:
        for i in instances:
            data[(s,i)] = read_json(dirname+"/{}__{}.json".format(s,i))
    print("instance;v*;gurobi (s);picat (s);ortools (s);gecode (s);chuffed (s)")
    for i in instances:
        print("{};{};{};{};{};{};{}".format(
            i,
            data[("gurobi",i)]["obj"],
            "-" if data[("gurobi",i)]["status"] != "ok"     else "{:.3f}".format(data[("gurobi",i)]["time"]),
            "-" if data[("picat",i)]["status"] != "ok"      else "{:.3f}".format(data[("picat",i)]["time"]),
            "-" if data[("ortools",i)]["status"] != "ok"    else "{:.3f}".format(data[("ortools",i)]["time"]),
            "-" if data[("gecode",i)]["status"] != "ok"     else "{:.3f}".format(data[("gecode",i)]["time"]),
            "-" if data[("chuffed",i)]["status"] != "ok"    else "{:.3f}".format(data[("chuffed",i)]["time"])
        ))

if __name__ == "__main__":
    if len(argv) < 2:
        print("USAGE {} DIRNAME".format(argv[0]))
        exit(1)
    else:
        main(argv[1])