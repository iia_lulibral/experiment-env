#!/bin/bash

for executable; do
    for dep in $(ldd "$executable"  | tr -d '\t' | cut -d ' ' -f 3); do
        name=$(ldd "$executable" | grep "$dep" | tr -d '\t' | cut -d ' ' -f 1)
        name=$(basename "$name")
        shortdep=$(basename "$dep")
        if [ ! -e "$shortdep" ]; then
            echo "Copying $name"
            cp "$dep" .
            chmod +x "$shortdep"
            if [ "$shortdep" != "$name" ]; then
                echo ln -s "$dep" "$name"
            fi
        else
            echo "$shortdep already here"
        fi
    done
done
