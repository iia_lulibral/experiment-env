int: R; % number of rounds
int: KEY_BITS; % key length {128, 192, 256}
int: KC = KEY_BITS div 32; % number of columns per round of key schedule (in {4,6,8})

set of int: Rounds = 0..R-1;
set of int: AllRounds = 0..R;
set of int: Bool = 0..1;

array[Rounds] of int: C = array1d(Rounds, [ 1 | i in Rounds ]);

array[0..3, 0..3] of 0..3: SUB1 = array2d(0..3, 0..3, [
  0, 2, 1, 3,
  2, 0, 3, 1,
  1, 3, 0, 2,
  3, 1, 2, 0
]);

array[0..3, 0..3] of 0..3: SUB2 = array2d(0..3, 0..3, [
  0, 3, 2, 1,
  2, 1, 0, 3,
  1, 2, 3, 0,
  3, 0, 1, 2
]);

%%% VARIABLES
var 1..KEY_BITS div 6: obj;

array[AllRounds, 0..3, 0..3] of var Bool: DX;  % state after ARK
array[AllRounds, 0..3, 0..3] of var Bool: DK;  % key
array[Rounds,    0..3, 0..3] of var Bool: DSX; % state after SB
array[Rounds,    0..3, 0..3] of var Bool: DY;  % state after SR
array[Rounds,    0..3, 0..3] of var Bool: DZ;  % state after MC



%%% GENERAL-PURPOSE PREDICATES

predicate XOR(var Bool: a, var Bool: b, var Bool: c) = a+b+c != 1;

predicate XOR3(var Bool: a, var Bool: b, var Bool: c, var Bool: res) = a+b+c+res != 1;

test isSBRound(int:i) = ( % True if a column of the key goes through SBoxes at round i
      (KEY_BITS==128) %AES 128 : all rounds
  \/  (KEY_BITS==192 /\ i mod 3 != 0) % AES 192: Rounds that are not multiples of 3
  \/  (KEY_BITS==256 /\ i>0) %AES 256: All rounds except the first one
);

% OBJECTIVE
% all DX and 4th column of DK
constraint obj = sum(r in AllRounds, i in 0..3, j in 0..3) ( DX[r,i,j] ) + sum(r in AllRounds, i in 0..3)( DK[r,i,3] );


 % SUB BYTES (SB) DX -> DSX
constraint forall(r in Rounds, i in 0..3, j in 0..3) (
  DSX[r,i,j] = DX[r,SUB1[i,j], SUB2[i,j]]
);

 
   
% ADD ROUND KEY (ARK)
constraint forall(r in Rounds, i in 0..3, j in 0..3)(
  XOR(DZ[r,i,j], DK[r,i,j], DX[r+1,i,j])
);



% SHIFT ROWS (SR) DSX -> DY
constraint forall(i in Rounds, j in 0..3, k in 0..3) (
  DY[i,j,k] = DSX[i,j,(k+j) mod 4]
);


% MIX COLUMNS (MC) DY -> DZ
set of int: MCRounds = 0..R-2;
constraint forall(r in MCRounds, j in 0..3) (
  XOR3(DY[r,1,j], DY[r,2,j], DY[r,3,j], DZ[r,0,j]) /\
  XOR3(DY[r,0,j], DY[r,2,j], DY[r,3,j], DZ[r,1,j]) /\
  XOR3(DY[r,0,j], DY[r,1,j], DY[r,3,j], DZ[r,2,j]) /\
  XOR3(DY[r,0,j], DY[r,1,j], DY[r,2,j], DZ[r,3,j])
);
constraint forall(i in 0..3, j in 0..3) (
  DZ[R-1, i, j] == DY[R-1, i, j]
);
% crypto improvement 1 ?
constraint forall(r in MCRounds, j in 0..3) (
  sum(i in 0..3)(DY[r,i,j]+DZ[r,i,j]) in {0,4,5,6,7,8}
);
% crypto improvement 2 ?
constraint forall(r in MCRounds, j in 0..3) (
  sum(i in 0..3)(DZ[r,i,j])==0 <-> sum(i in 0..3)(DY[r,i,j])==0
);


% KEY SCHEDULE (KS) key length K=128

% ∀i ∈ [0, r − 1], ∀j ∈ [0, 3], SK i [j][3] = S(K i [j][3]).
constraint forall(i in 0..R-2, j in 0..3)(
  DK[i+1,j,3] = DK[i,SUB1[j,3],SUB2[j,3]]
);

%∀i ∈ [0, r − 1], K i+1 [0][0] = SK i [1][3] ⊕ K i [0][0] ⊕ c i
constraint forall(i in Rounds)(
  XOR3(DK[i+1,0,0], DK[i,1,3], DK[i,0,0], C[i])
);

%∀i ∈ [0, r − 1], ∀j ∈ [1, 3], K i+1 [j][0] = SK i [(j + 1)%4][3] ⊕ K i [j][0].
constraint forall(i in Rounds, j in 1..3)(
  DK[i+1,j,0] == (DK[i,(j+1) mod 4,3]+DK[i,j,0]) mod 2
);

%∀i ∈ [0, r−1], ∀j ∈ [0, 3], ∀k ∈ [1, 3], K i+1 [j][k] = K i+1 [j][k−1] ⊕ K i [j][k].
constraint forall( i in Rounds, j in 0..3, k in 1..3)(
  DK[i+1,j,k] == (DK[i+1,j,k-1]+DK[i,j,k]) mod 2
);


%%% SOLVE %%%
solve minimize obj;