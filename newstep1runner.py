#!/usr/bin/python3
from sys import argv
from subprocess import Popen, PIPE
import os
import shlex
import time
import json

def syscall(command):
    """
    calls a system command and returns the code, standard output, error output
    """
    process = Popen(shlex.split(command), stdout=PIPE)
    (out,err) = process.communicate()
    code = process.wait()
    return code, out, err

def write_obj_mzn(filename, objval):
    with open(filename,"w") as f:
        f.write("constraint obj = {};".format(objval))

def write_enum_mzn(filename, sboxes, writeflag):
    symbol_list = []
    for k in sboxes.keys():
        if sboxes[k] == 0:
            symbol_list.append("(1-{})".format(k))
        else:
            symbol_list.append("{}".format(k))
    with open(filename,writeflag) as f:
        f.write("constraint {} <= {}-1;\n".format(" + ".join(symbol_list), len(symbol_list)))


def picat_solve_opt(filename, prefix):
    """
    solves the step1opt using picat
    """
    # create step1opt.mzn file
    with open("step1opt.mzn","w") as f:
        f.write("solve minimize obj;")
    # create fzn
    code,out,err = syscall(
        prefix+"external/minizinc-2-5-0/bin/minizinc -c {} step1opt.mzn"
        .format(filename)
    )
    # solve using picat
    code,out,err = syscall(
        "{}/external/solvers/picat/picat\
        {}/external/solvers/picat/fzn_picat_sat.pi\
        {} -v".format(prefix, prefix, filename.replace(".mzn",".fzn"))        
    )
    # get objective
    s = str(out).split('----------')[-2]
    sobj = s.split("\\n")[1].split(" = ")[1]
    # get active sboxes
    ssbox = s.split("\\n")[2:]
    ssbox = map(lambda e: e.replace(";","").split(" = "), ssbox)
    ssbox = filter(lambda e:e!=[''], ssbox)
    res_sbox = dict()
    for e in ssbox:
        res_sbox[e[0]] = int(e[1])
    return {"obj":int(sobj), "sboxes":res_sbox}



def picat_solve_enum(filename, prefix):
    """
    solves the step1opt using picat
    """
    # create fzn
    code,out,err = syscall(
        prefix+"external/minizinc-2-5-0/bin/minizinc -c {} {} {}"
        .format(
            filename,
            filename.replace(".mzn", "_enum.mzn"),
            filename.replace(".mzn", "_obj.mzn")
        )
    )
    # solve using picat
    code,out,err = syscall(
        "{}/external/solvers/picat/picat\
        {}/external/solvers/picat/fzn_picat_sat.pi\
        {}".format(prefix, prefix, filename.replace(".mzn",".fzn"))        
    )
    # get objective
    if "=====UNSATISFIABLE=====" in str(out):
        return {"status":"unsat"}
    else:
        s = str(out).split('----------')[-2]
        # get active sboxes
        ssbox = s.split("\\n")[2:]
        ssbox = map(lambda e: e.replace(";","").split(" = "), ssbox)
        ssbox = filter(lambda e:e!=[''], ssbox)
        res_sbox = dict()
        for e in ssbox:
            res_sbox[e[0]] = int(e[1])
        return {"status":"sat", "v":res_sbox}



def newstep1runner(filename, env_prefix, output_filename):
    t1 = time.time()
    step1obj = picat_solve_opt(filename, env_prefix)
    write_obj_mzn(filename.replace(".mzn", "_obj.mzn"), step1obj["obj"])
    write_enum_mzn(filename.replace(".mzn", "_enum.mzn"), step1obj["sboxes"], "w")
    all_sols = [step1obj["sboxes"]]
    print("OBJ: {}".format(step1obj["obj"]))
    print("{} sols".format(len(all_sols)))
    t2 = time.time()
    print("step1opt time: {:.2f}".format(t2-t1))
    while True:
        sol = picat_solve_enum(filename, env_prefix)
        if sol["status"] == "unsat":
            break
        else:
            all_sols.append(sol["v"])
            write_enum_mzn(filename.replace(".mzn", "_enum.mzn"), sol["v"], "a")
            print("{} sols".format(len(all_sols)))
    t3 = time.time()
    print("step1enum time: {:.2f}".format(t3-t2))
    # write json results file
    with open(output_filename, 'w') as f:
        json.dump({
            'topt':t2-t1,
            'tenum':t3-t2,
            'v':step1obj["obj"],
            'sols':all_sols
        }, f)

if __name__ == "__main__":
    if len(argv) < 4:
        print("USAGE: {} STEP1.MZN ENV_PREFIX OUTPUT_JSON".format(argv[0]))
        print("\TENV_PREFIX example: '~/experiment-env/'")
        exit(1)
    else:
        newstep1runner(argv[1], argv[2], argv[3])