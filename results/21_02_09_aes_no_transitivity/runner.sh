#!/bin/sh
for R in 3 4 5
do
    tsp ../../newstep1runner.py "../../experiments/aes/diff_aes128_$R.mzn" "../../" diff_aes128_"$R"_results.json
done
for R in 3 4 5 6 7 8 9 10 11 12 13 14
do
    tsp ../../newstep1runner.py "../../experiments/aes/diff_aes256_$R.mzn" "../../" diff_aes256_"$R"_results.json
done
