# experiment-env/results

contains numerical results obtained by our runs. Each folder corresponds to a batch run with the name of the experiment, the date and the commit version.