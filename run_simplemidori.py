import os
from subprocess import Popen, PIPE
import shlex
import re
import json
import time

from experiments import get_experiment_id, run_parallel


solvers = ["gurobi", "picat", "gecode", "chuffed", "ortools"]
# solvers = ["gurobi"]
instances = ["midori_{}".format(i) for i in range(3,21)]


def get_result_stats(solver, instance, res):
    output = res["out"].decode("utf-8")
    err = res["err"]
    code = res["code"]
    if err:
        return {"status": "error", "why": err}
    res = {"status": "ok", "solver": solver, "instance": instance}
    # print(output)
    try:
        res["time"] = float(re.search('solveTime=(.*?)\n', output).group(1))
        res["nb_flat_intvars"] = int(re.search('flatIntVars=(.*?)\n', output).group(1))
        res["nb_flat_constraints"] = int(re.search('flatIntConstraints=(.*?)\n', output).group(1))
        res["nb_flat_boolvars"] = 0
        boolvar_re = re.search('flatBoolVars=(.*?)\n', output)
        if boolvar_re:
            res["nb_flat_boolvars"] = int(boolvar_re.group(1))
        # res["nodes"] = int(re.search('nodes=(.*?)\n', output).group(1))
        # res["opened_nodes"] = int(re.search('openNodes=(.*?)\n', output).group(1))
        res["obj"] = int(re.findall('objective\s*=\s*(.*?)[\n|;]', output)[-1])
    except:
        # print(output)
        return {"status": "error", "why": "could not parse output", "solver": solver, "instance": instance, "output": output}
    return res




def runtask(solver,instance,outputdir):
    print("RUN: {} {}".format(solver, instance))
    # run minizinc for the given solver
    prep_command = {"cmd": ""}
    res = {"out": "", "err": "", "code": ""}

    if solver == "gurobi":
        #prep_command["cmd"] = "external/minizinc-2-5-0/bin/minizinc --solver gurobi --gurobi-dll /opt/gurobi903/lib/libgurobi90.so -s --time-limit 3600000 models/simplemidori/{}.mzn".format(instance)
        prep_command["cmd"] = "external/minizinc-2-5-0/bin/minizinc --solver gurobi --gurobi-dll ~/gurobi950/linux64/lib/libgurobi.so.9.5.0 -s --time-limit 3600000 models/simplemidori/{}.mzn".format(instance)
  
    if solver == "chuffed":
        prep_command["cmd"] = "external/minizinc-2-5-0/bin/minizinc --solver external/solvers/chuffed/chuffed.msc -s --time-limit 3600000 models/simplemidori/{}.mzn".format(instance)
    if solver == "ortools":
        prep_command["cmd"] = "external/minizinc-2-5-0/bin/minizinc --solver external/solvers/ortools/ortools.msc -s --time-limit 3600000 models/simplemidori/{}.mzn".format(instance)
    if solver == "gecode":
        prep_command["cmd"] = "external/minizinc-2-5-0/bin/minizinc -s --time-limit 3600000 models/simplemidori/{}.mzn".format(instance)
    if solver == "picat":
        # generate fzn file
        process = Popen(shlex.split("external/minizinc-2-5-0/bin/minizinc -c models/simplemidori/{}.mzn".format(instance)), stdout=PIPE)
        out,err = process.communicate()
        code = process.wait()
        # define solver command
        prep_command["cmd"] = "external/solvers/picat/picat external/solvers/picat/fzn_picat_sat.pi models/simplemidori/{}.fzn".format(instance)
        
    start = time.time()
    process = Popen(shlex.split(prep_command["cmd"]), stdout=PIPE)
    (res["out"],res["err"]) = process.communicate()
    res["code"] = process.wait()
    end = time.time()
    if solver == "picat":
        print("END: {} {}".format(solver, instance))
        res = {"status": "ok", "time": end-start, "solver": solver, "instance": instance}
    else:
        res = get_result_stats(solver, instance, res)
    print("END: {} {}".format(solver, instance))
    with open("{}/{}__{}.json".format(outputdir,solver,instance), "w") as f:
        f.write(json.dumps(res))
        f.write("\n")




def createtask(solver,instance,outputdir):
    return lambda: runtask(solver, instance, outputdir)




def main():
    exp_id = get_experiment_id()
    print("=== STARTING EXPERIMENT {} ===".format(exp_id))
    output_dir = "results/simplemidori__{}".format(exp_id)
    print("CREATING RESULT DIRECTORY ({})".format(output_dir))
    os.mkdir(output_dir)
    print("RUNNING EXPERIMENTS")
    tasks = []
    for i in instances:
        for s in solvers:
            tasks.append(createtask(s, i, output_dir))
    run_parallel(tasks)
    print("=== EXPERIMENT FINISHED: {} ===".format(exp_id))


if __name__ == "__main__":
    main()