echo "======================== UPDATING SYSTEM ========================"
apt-get update
apt-get upgrade
apt-get install curl
apt-get install git
apt-get install task-spooler
apt-get install libssl-dev zlib1g-dev
apt-get install htop
echo "=======================  INSTALLING RUST ========================"
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
echo "=================== INSTALLING TAGADA DEPENDENCIES ========================"
apt-get install cargo
