#!/usr/bin/python3
from experiments import get_experiment_id, run_parallel, execute_program
import os

if __name__ == "__main__":
    exp_id = get_experiment_id()
    output_dir = "results/testexpcommand__{}".format(exp_id)
    if os.path.exists(output_dir):
        print("DIRECTORY {} ALREADY EXISTS".format(output_dir))
    else:
        print("CREATING RESULT DIRECTORY ({})".format(output_dir))
        os.mkdir(output_dir)
    print("RUNNING EXPERIMENTS")
    tasks = [
        lambda: execute_program(output_dir, "echo 'a'", "log_a.txt", "completed_list.txt"),
        lambda: execute_program(output_dir, "echo 'b'", "log_b.txt", "completed_list.txt"),
        lambda: execute_program(output_dir, "echo 'c'", "log_c.txt", "completed_list.txt"),
    ]
    run_parallel(tasks)
    print("=== EXPERIMENT FINISHED: {} ===".format(exp_id))