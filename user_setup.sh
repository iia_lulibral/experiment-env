echo "=================== CLONING REPOSITORIES ========================"
git clone https://gitlab.limos.fr/iia_lulibral/experiment-env
git clone https://gitlab.limos.fr/iia_lulibral/tagada
echo "=================== INSTALLING RUBY ========================"
git clone https://github.com/rbenv/rbenv.git ~/.rbenv
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(rbenv init -)"' >> ~/.bashrc
source ~/.bashrc
git clone https://github.com/rbenv/ruby-build.git "$(rbenv root)"/plugins/ruby-build
rbenv install 2.7.1
rbenv global 2.7.1
echo "=================== INSTALLING RUBY GEMS ========================"
gem install minitest
gem install colorize
gem install rutie
gem install rgl
gem install union_find
gem install pry
echo "=================== COMPILING RUST PART ========================"
cd tagada/rust/
sudo cargo build --release
cd ../../
echo "=================== SET UP TASK SPOOLER ========================"
tsp -S 20